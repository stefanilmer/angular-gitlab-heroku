import { switchMap } from 'rxjs/operators'
import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { Observable } from 'rxjs'

import { Hero } from '../hero/hero' // Import hero class
import { HeroService } from '../hero-service/hero.service' // Import hereo Service

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {
  hero$: Observable<Hero>

  constructor(private route: ActivatedRoute, private router: Router, private heroService: HeroService) {}

  ngOnInit() {
    this.hero$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.heroService.getHero(params.get('id')))
    )
  }

  gotoHeroes() {
    this.router.navigate(['/heroes'])
  }
}
