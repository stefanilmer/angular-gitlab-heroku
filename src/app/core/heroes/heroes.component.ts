import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { Observable } from 'rxjs'
import { switchMap } from 'rxjs/operators'

import { Hero } from './hero/hero'
import { HeroService } from './hero-service/hero.service'

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {
  heroes$: Observable<Hero[]>
  selectedId: number

  constructor(private heroService: HeroService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.heroes$ = this.route.paramMap.pipe(
      switchMap(params => {
        // (+) before `params.get()` turns the string into a number
        this.selectedId = +params.get('id')
        return this.heroService.getHeroes()
      })
    )
  }

  // getHeroes(): void {
  //   this.heroService.getHeroes().subscribe(heroes => (this.heroes = heroes))
  // }
}
