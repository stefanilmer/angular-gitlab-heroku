export class Hero {
  id: number
  name: string
  gender: string
  gold: number
  heroPower: number
}
// createdDate:    Date;
// inventory:      string[];
// gear:           string[];
