import { Hero } from './hero/hero'

export const HEROES: Hero[] = [
  { id: 1, name: 'windForce', gender: 'China', heroPower: 999, gold: 999 },
  { id: 2, name: 'Spider woman', gender: 'Female', heroPower: 55, gold: 213 },
  { id: 3, name: 'Lightning', gender: 'Male', heroPower: 29, gold: 392 },
  { id: 4, name: 'brokko', gender: 'Male', heroPower: 12, gold: 1500 },
  { id: 5, name: 'Fem', gender: 'Female', heroPower: 1, gold: 1 },
  { id: 6, name: 'adHoc', gender: 'Unknown', heroPower: 66, gold: 85 },
  { id: 7, name: 'STAN', gender: 'Unknown', heroPower: 666, gold: 666 },
  { id: 8, name: 'Falcon', gender: 'Male', heroPower: 51, gold: 256 }
]
