import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { QuestListComponent } from './core/quests/quest-list/quest-list.component'
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component'

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: UsecasesComponent },
  { path: 'quests', component: QuestListComponent },
  { path: '**', component: PageNotFoundComponent } // MOET ALTIJD ALS LAATSTE!
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
